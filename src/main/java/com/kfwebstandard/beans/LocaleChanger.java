package com.kfwebstandard.beans;

import java.util.Locale;
import java.util.Map;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to switch the locale which changes the language
 *
 * @author Ken
 */
@Named
@RequestScoped
public class LocaleChanger {

    private final static Logger LOG = LoggerFactory.getLogger(LocaleChanger.class);

    public String changeLocale() {
        FacesContext context = FacesContext.getCurrentInstance();
        //String languageCode = getLanguageCode(context);
        
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String languageCode = params.get("languageCode");        

        Locale aLocale;
        switch (languageCode) {
            case "en_CA":
                aLocale = Locale.CANADA;
                break;
            case "fr_CA":
                aLocale = Locale.CANADA_FRENCH;
                break;
            default:
                aLocale = Locale.getDefault();
        }
        context.getViewRoot().setLocale(aLocale);
        return null;
    }

    private String getLanguageCode(FacesContext context) {
        Map<String, String> params = context.getExternalContext()
                .getRequestParameterMap();
        return params.get("languageCode");
    }
}
